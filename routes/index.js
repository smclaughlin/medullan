/*
 * GET home page.
 */

var games = {};


module.exports = function(app){
	app.get('/', function(req, res){
		res.render('index', { title: 'Express' });
	});

	app.post('/connect', function(req,res){

		var username = req.session.username = req.body.username;
		var gamename = req.session.gamename = req.body.gamename;
		
		if(typeof games[gamename] == "undefined"){
			console.log("creating new game");

			games[gamename] = {};
			games[gamename]["moves"] = [];
			games[gamename]["players"] = []; 

			games[gamename]["players"].push(username);
			res.send({"username" :  username, "gamename" : gamename});

		} else {
			games[gamename]["players"].push(username);
			res.send({"username" :  username, "gamename" : gamename});
			
		}
		
		
	});

	app.post('/move', function(req,res){
		var username = req.session.username;
		var gamename = req.session.gamename;
		
		var players = games[gamename]["players"];

		
		var playerNumber = players.indexOf(username);

		
		var moveObj = {"row" : req.body.row, "column" : req.body.column, "gamename" : gamename, "username" : username, "playerNumber" : playerNumber};
		
		games[gamename]["moves"].push(moveObj);
		
		
		
		res.send(games[gamename]["moves"]);
	});

	app.get('/current/:gamename', function(req, res){
		res.send(games[req.params.gamename]["moves"])
	});

}
